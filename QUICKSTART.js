//const esc = require('email-format-check');
const esc = require('./');

esc.syntaxCheck("mehmet.kozan@liev.com").then(function(res){
    console.log(res.status);//none
});

//enable did you mean
esc.syntaxCheck("mehmet.kozan@liev.com",true).then(function(res){
    console.log(res.did_you_mean);//mehmet.kozan@live.com
    console.log(res.status);//free-email
});

esc.syntaxCheck("career@cia.gov").then(function(res){
    console.log(res.status);//official
});

esc.syntaxCheck("info@microsoft.com").then(function(res){
    console.log(res.status);//role-based
});

esc.syntaxCheck("bouncer@company.com").then(function(res){
    console.log(res.status);//spam-trap
});

esc.syntaxCheck("tom@10minutemail.com").then(function(res){
    console.log(res.status);//disposable
});




