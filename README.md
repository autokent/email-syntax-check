# email-syntax-check
> **E-Mail address syntax validation module.**

![logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/4802406/email-syntax-check.png)

[![version](https://img.shields.io/npm/v/email-syntax-check.svg)](https://www.npmjs.org/package/email-syntax-check)
[![downloads](https://img.shields.io/npm/dt/email-syntax-check.svg)](https://www.npmjs.org/package/email-syntax-check)
[![node](https://img.shields.io/node/v/email-syntax-check.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/email-syntax-check/badges/master/pipeline.svg)](https://gitlab.com/autokent/email-syntax-check/pipelines)

## Installation
`npm install email-syntax-check`

## Status
`none,official,disposable,free-email,spam-trap,role-based`

## Usage
```js
const esc = require('email-syntax-check');

esc.syntaxCheck("mehmet.kozan@liev.com").then(function(res){
    console.log(res.status);//none
});

//enable did you mean
esc.syntaxCheck("mehmet.kozan@liev.com",true).then(function(res){
    console.log(res.did_you_mean);//mehmet.kozan@live.com
    console.log(res.status);//free-email
});

esc.syntaxCheck("career@cia.gov").then(function(res){
    console.log(res.status);//official
});

esc.syntaxCheck("info@microsoft.com").then(function(res){
    console.log(res.status);//role-based
});

esc.syntaxCheck("bouncer@company.com").then(function(res){
    console.log(res.status);//spam-trap
});

esc.syntaxCheck("tom@10minutemail.com").then(function(res){
    console.log(res.status);//disposable
});

```


## Test
`mocha` or `npm test`

> check test folder and QUICKSTART.js for extra usage.