'use strict';

const mailcheck = require("mailcheck");
const co = require('co');
const freemail = require('freemail');
const fs = require('fs');
const easymatch = require('easy-match');
const disposable_email = require('disposable-email');
const disposable_email_domains = require('disposable-email-domains');
const disposable_email_domains_wildcard = require('disposable-email-domains/wildcard.json');
const free_email_domains = require('free-email-domains');
const role_based_email_addresses = require('role-based-email-addresses');

const spamtrapArr = fs.readFileSync(__dirname + '/data/spam-trap.txt').toString().replace(/\r/g,'').split('\n');
const officialArr = fs.readFileSync(__dirname + '/data/official.txt').toString().replace(/\r/g,'').split('\n');
const rolebasedArr = fs.readFileSync(__dirname + '/data/role-based.txt').toString().replace(/\r/g,'').split('\n');

function didYouMean(email){
    return new Promise(function(resolve, reject) {  
        mailcheck.run({
            email:email,
            suggested: function(suggestion) {
                // callback code 
                resolve(suggestion.full);
            },
            empty: function() {
                // callback code 
                resolve(null);
            }
        });
    });
}

function email_misspell_check(email){
    return new Promise(function(resolve, reject) {  
        mailcheck.run({
            email:email,
            suggested: function(suggestion) {
                // callback code 
                resolve(suggestion.full);
            },
            empty: function() {
                // callback code 
                resolve(null);
            }
        });
    });
}

function email_spam_check(email){
    let ret = false;

    if(! ret){
        let res = easymatch(email,spamtrapArr);
        if(res.ismatch) ret = true;
    }

    return ret;
}

function email_official_check(email){
    let ret = false;

    if(! ret){
        let res = easymatch(email,officialArr);
        if(res.ismatch) ret = true;
    }

    return ret;
}

function email_free_check(email){
    let ret = false;
    let parts = email.split('@');
    let name = parts[0];
    let domain = parts[1];

    if(! ret) ret = freemail.isFree(email);
    if(! ret) ret = free_email_domains.includes(domain);

    return ret;
}

function email_disposable_check(email){
    let ret = false;
    let parts = email.split('@');
    let name = parts[0];
    let domain = parts[1];

    if(! ret) ret = freemail.isDisposable(email);
    if(! ret) ret = ! disposable_email.validate(email);
    if(! ret) ret = disposable_email_domains.includes(domain);
    if(! ret) ret = disposable_email_domains_wildcard.includes(domain);

    return ret;
}

function email_role_check(email){
    let ret = false;
    let name = email.split('@')[0];

    if(! ret) ret = role_based_email_addresses.includes(name);
    if(! ret){
        let res = rolebasedArr.includes(name);
        if(res.ismatch) ret = true;
    }

    return ret;
}

let syntaxCheck = co.wrap(function* (email,check_misspell) {
    let ret ={email:email,did_you_mean:null,status:"none"};//none,official,disposable,free-email,spam-trap,role-based
    
    if(typeof check_misspell == "undefined") check_misspell = false;
    if(typeof check_misspell != "boolean") check_misspell = false;

    if(check_misspell){
        let did_you_mean = yield email_misspell_check(email);

        if(did_you_mean !=null ){
            ret.did_you_mean = did_you_mean;
            email = did_you_mean;
        }
    }

    if(email_spam_check(email)){
        ret.status = "spam-trap";
    }
    else if(email_official_check(email)){
        ret.status = "official";
    }
    else if(email_disposable_check(email)){
        ret.status = "disposable";
    }
    else if(email_free_check(email)){
        ret.status = "free-email";
    }
    else if(email_role_check(email))
    {
        ret.status = "role-based";
    }

    return ret;
});

module.exports.isFree = email_free_check;
module.exports.isDisposable = email_disposable_check;
module.exports.isSpamTrap = email_spam_check;
module.exports.isRoleBased = email_role_check;
module.exports.isOfficial = email_official_check;
module.exports.isFree = email_free_check;
module.exports.didYouMean = didYouMean;
module.exports.syntaxCheck =  syntaxCheck;

//for testing purpose
if (!module.parent) 
{
    let testing_purpose = co.wrap(function* (){
        let res_didYouMean = yield didYouMean("mehmet.kozan@hotmali.com");
        debugger;

        let res_syntaxCheck = yield syntaxCheck("mehmet.kozan@hotmali.com",true);
        debugger;

    });

    testing_purpose();
}