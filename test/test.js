const assert = require('assert');
const esc = require('../');

describe('general test', function() {
    it('should none', function() {
        return esc.syntaxCheck("mehmet.kozan@liev.com")
        .then(function(result){
            assert.equal(result.status,"none");
        });
    });

    it('should free-email', function() {
        return esc.syntaxCheck("mehmet.kozan@liev.com",true)
        .then(function(result){
            assert.equal(result.status,"free-email");
        });
    });

    it('should official', function() {
        return esc.syntaxCheck("career@cia.gov")
        .then(function(result){
            assert.equal(result.status,"official");
        });
    });

    it('should role-based', function() {
        return esc.syntaxCheck("info@microsoft.com")
        .then(function(result){
            assert.equal(result.status,"role-based");
        });
    });

    it('should spam-trap', function() {
        return esc.syntaxCheck("bouncer@company.com")
        .then(function(result){
            assert.equal(result.status,"spam-trap");
        });
    });

    it('should disposable', function() {
        return esc.syntaxCheck("tom@10minutemail.com")
        .then(function(result){
            assert.equal(result.status,"disposable");
        });
    });
});


